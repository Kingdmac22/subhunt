﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

    [SerializeField] Slider healthBar;
    private int playerHealth;
    private Vector3 playerStart;


	// Use this for initialization
	void Start () {
        playerHealth = 100;
        playerStart = gameObject.transform.position;
	}
	
	public void DamagePlayer(int damageAmount)
    {
        playerHealth = playerHealth - damageAmount;
        if(playerHealth > 0)
        {
            healthBar.value = playerHealth;

        }
        else
        {
           
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
            gameObject.GetComponent<Rigidbody2D>().freezeRotation = false;
            gameObject.GetComponent<Rigidbody2D>().AddTorque(300);
            healthBar.value = 0;
            playerHealth = 0;
            Invoke("resetPlayer", 2);
            
        }
    }

    public void resetPlayer()
    {
        gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0f;
        gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        gameObject.transform.position = playerStart;
        playerHealth = 100;
        healthBar.value = 100;
    }

}
