﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {
    private Rigidbody2D rb;
    private float speedX;
    private Transform target;
    private GameObject playerCharacter;
    private Transform myTransform;
    public int enemyNumber;

    [SerializeField] GameObject enemyTorpeedo;

    private int enemyPoints;

    // Use this for initialization
    [SerializeField] GameObject ememyExplode;
    public bool directionRight;
    private bool hasFired;

    float nextActionTime;
    float firePeriod;

    void Start () {
        switch (enemyNumber)
        {

            case 3:
                firePeriod = Random.Range(4f, 10f);
                speedX = 2;
                enemyPoints = 20;
                break;
            case 2:
                firePeriod = Random.Range(4f, 10f);
                speedX = 3;
                enemyPoints = 30;
                break;
            case 1:
                firePeriod = Random.Range(4f, 6f);
                enemyPoints = 50;
                speedX = 4;
                break;
            default:
                print("Incorrect intelligence level.");
                break;
        }

       rb = GetComponent<Rigidbody2D>();
      
       nextActionTime = firePeriod;
       playerCharacter = GameObject.FindGameObjectWithTag("Player");
       hasFired = false;
    }

    //Move Enemy Sub
    private void FixedUpdate()
    {
        if (directionRight)
        {
            rb.velocity = new Vector2(speedX, 0f);
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
        } else
        {
            rb.velocity = new Vector2(speedX * -1, 0f);
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
    }


    // Update is called once per frame
    /*void FixedUpdate()
    {

        if (directionRight)
        {
            rb.AddForce(movement * speed);
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
            gameObject.GetComponent<SpriteRenderer>().flipY = false;

        } else
        {
            rb.AddForce(-1 * movement * speed);
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
            gameObject.GetComponent<SpriteRenderer>().flipY = true;

        }

    }*/


    private void Update()
    {

       // transform.position += transform.right * speed * Time.deltaTime;
       

        if (Time.time > nextActionTime)
        {
            nextActionTime = Time.time + firePeriod;
            if (!hasFired)
            {
                ShootAtPlayer();
            }
           
        }
    }


    void ShootAtPlayer()
    {
        //target = playerCharacter.transform;
        Debug.Log("Torpeedo Fired");
        // rotate the projectile to aim the target:
        GameObject fireTorpeedp;
        fireTorpeedp = Instantiate(enemyTorpeedo, transform.position, transform.rotation) as GameObject;
        fireTorpeedp.transform.position = gameObject.transform.position;
        hasFired = true;

    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Torpeedo")
        {
            GameObject Explosion;
            Explosion = Instantiate(ememyExplode, transform.position, transform.rotation) as GameObject;
            Explosion.transform.position = transform.position;
            GameObject playerScore;
            playerScore = GameObject.FindGameObjectWithTag("PlayerScore");
            playerScore.GetComponent<GameScore>().updateScore(enemyPoints);
            Destroy(col.gameObject);
            Destroy(gameObject);

        }

        if (col.gameObject.tag == "Boundary")
        {
            Destroy(gameObject);

        }
    }
}
