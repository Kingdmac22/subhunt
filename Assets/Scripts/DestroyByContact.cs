﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Torpeedo")
        {
            Destroy(col.gameObject);
            
        }
        if (col.gameObject.tag == "Enemy")
        {
            Destroy(col.gameObject);

        }
    }
    
}
