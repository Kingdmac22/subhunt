﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScore : MonoBehaviour {
    public int PlayerScore;
    [SerializeField] Text ScoreText;
	// Use this for initialization
	void Start () {
        PlayerScore = 0;
        ScoreText.text = "0";
    }
	
    public void updateScore(int Points)
    {
        PlayerScore = PlayerScore + Points;
        ScoreText.text = PlayerScore.ToString();
    }

}
