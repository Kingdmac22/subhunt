﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    [SerializeField] float period;
    float nextActionTime;
    [SerializeField] GameObject enemy;
    [SerializeField] Transform[] spawnPoints;
    float enemyScale;
    int enemySpawnNumber;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > nextActionTime)
        {
            nextActionTime = Time.time + period;
            GameObject spawnEnemy;
            int spawnPoint;
            spawnPoint = Random.Range(1, 8);
            int randomEnemy = Random.Range(1,4);

            Debug.Log("Random Enemy Number " + randomEnemy);
            switch (randomEnemy)
            {

                case 3:
                    enemyScale = 0.3f;
                    enemySpawnNumber = 3;
                    break;
                case 2:
                    enemyScale = 0.2f;
                    enemySpawnNumber = 2;
                    break;
                case 1:
                    enemyScale = 0.1f;
                    enemySpawnNumber = 1;
                    break;
                default:
                    print("Incorrect intelligence level.");
                    break;
            }

            if (spawnPoint > 4)
            {
                
                spawnEnemy = Instantiate(enemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation) as GameObject;
                spawnEnemy.transform.position = spawnPoints[spawnPoint].position;
                spawnEnemy.transform.rotation = spawnPoints[spawnPoint].rotation;
                spawnEnemy.transform.localScale = new Vector3 (enemyScale, enemyScale, 1);
                spawnEnemy.GetComponent<EnemyManager>().directionRight = false;
                ColourEnemy(enemySpawnNumber, spawnEnemy);
                spawnEnemy.GetComponent<EnemyManager>().enemyNumber = enemySpawnNumber;
            }
            else
            {
                spawnEnemy = Instantiate(enemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation) as GameObject;
                spawnEnemy.transform.position = spawnPoints[spawnPoint].position;
                spawnEnemy.transform.rotation = spawnPoints[spawnPoint].rotation;
                spawnEnemy.transform.localScale = new Vector3(enemyScale, enemyScale, 1);
                spawnEnemy.GetComponent<EnemyManager>().directionRight = true;
                ColourEnemy(enemySpawnNumber, spawnEnemy);
                spawnEnemy.GetComponent<EnemyManager>().enemyNumber = enemySpawnNumber;
            }

        }
    }

    private void ColourEnemy (int enemyNumber, GameObject spawnEnemy)
    {
        switch (enemyNumber)
        {

            case 3:
                spawnEnemy.GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            case 2:
                spawnEnemy.GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
            case 1:
                spawnEnemy.GetComponent<SpriteRenderer>().color = Color.red;
                break;
            default:
                print("Incorrect intelligence level.");
                break;
        }
    }
}
