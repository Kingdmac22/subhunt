﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {

    public float speed;

    private Rigidbody2D rb;
    [SerializeField] GameObject torpeedo;
    [SerializeField] Transform torpeedoBay;
    [SerializeField] float period;
    float nextActionTime;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        nextActionTime = 0f + period;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //Vector3 movement = new Vector3(moveHorizontal, 0.0f);//, moveVertical);
        transform.Translate(new Vector3(moveHorizontal, moveVertical) * Time.deltaTime * speed);
        //rb.AddForce(movement * speed);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") )
        {

            if (Time.time > nextActionTime)
            {
                nextActionTime = Time.time + period;
                GameObject fireTorpeedp;
                fireTorpeedp = Instantiate(torpeedo, transform.position, transform.rotation) as GameObject;
                fireTorpeedp.transform.position = torpeedoBay.position;
                fireTorpeedp.transform.rotation = torpeedoBay.transform.rotation;
            }

            
            //fireTorpeedp.transform.eulerAngles = new Vector3(0f, 90f, 0f);

        }
    }
}
