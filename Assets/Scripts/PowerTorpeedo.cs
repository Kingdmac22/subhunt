﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerTorpeedo : MonoBehaviour {

    private Rigidbody2D rb;
    private Vector3 movement;
    private Transform target;

    public float speed = 5f;
    public float rotateSpeed = 200f;
    public int torpedoHitPower;
    public GameObject torpeedpExplosion;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        movement = new Vector3(0, 0);
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }


    private void FixedUpdate()
    {
        Vector2 direction = (Vector2)target.position - rb.position;

        direction.Normalize();

        float rotateAmount = Vector3.Cross(direction, transform.up).z;

        rb.angularVelocity = -rotateAmount * rotateSpeed;

        rb.velocity = transform.up * speed;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            target.GetComponent<PlayerManager>().DamagePlayer(torpedoHitPower);
            GameObject explosion;
            explosion = Instantiate(torpeedpExplosion, transform.position, transform.rotation) as GameObject;
            explosion.transform.position = transform.position;
        }

          Destroy(gameObject);
    }
}
